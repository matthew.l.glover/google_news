from setuptools import setup

setup(name="Google News Search",
    version='0.01',
    description="Tools for Searching Google News",
    author="Matthew Lee Glover",
    author_email="matthew.l.glover@gmail.com",
    url="https://gitlab.com/matthew.l.glover/google_news",
    py_modules=["google_news", "article_grabber"],
    data_files=[('phantomjs-2.1.1-linux-x86_64/bin/', ['phantomjs-2.1.1-linux-x86_64/bin/phantomjs'])])