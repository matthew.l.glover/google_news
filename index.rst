.. Google News Search documentation master file, created by
   sphinx-quickstart on Fri Nov  2 22:07:48 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Google News Search's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: google_news
.. autoclass:: GoogleNewsSearch
    :members:
.. autoclass:: ArticleFinder
    :members:
.. automodule:: article_grabber
    :members:
    :inherited-members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
