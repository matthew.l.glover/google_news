import unittest
from google_news import *
from bs4 import BeautifulSoup, element

class GoogleNewsSearchTest(unittest.TestCase):
    """Test the GoogleNewsSearch class"""
    def setUp(self):
        self.test_search = GoogleNewsSearch("Data Science")
        self.test_link_element = BeautifulSoup("""<a class="ipQwMb Q7tWef" 
                                href="./articles/CBMiUWh0dHBzOi8vd3d3Lm5obC5jb20vcGVuZ3VpbnMvbmV3cy9ncmFudC10b29rLXRoZS1sb25nLXJvYWQtdG8tdGhlLW5obC9jLTI5OTU0OTU1NNIBAA">
                                <span>Grant took the long road to the NHL</span>
                                </a>
                            """).a
        html_file = open("test/test_html.html", 'r')
        self.test_html = html_file.read()
        self.test_html_articles = ArticleFinder(self.test_html)
        

    def test_articles_returned(self):
        """Tests that a list of articles are returned"""
        self.assertTrue(isinstance(self.test_html_articles.articles, list))
        self.assertGreater(len(self.test_html_articles.articles), 0)
        self.assertTrue(isinstance(self.test_search.articles, list))
        self.assertGreater(len(self.test_search.articles), 0)

    def test_is_article_function(self):
        self.assertTrue(is_article(self.test_link_element))

