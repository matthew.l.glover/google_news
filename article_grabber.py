import requests
import string
import pyspark
import google_news
import newspaper
from textblob import TextBlob
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from pandas import DataFrame
import argparse
import datetime



class Article(newspaper.Article):
    """This class is a subclass of newspaper.Article

    The Article class parses an article, then performs 
    sentiment analysis on the Article.

    Attributes:
    """
    def __init__(self, link):
        super(Article, self).__init__(link)
        self.results_dict = {}
        try:
            self.download()
            self.parse()
            #self.translator = str.maketrans('', '', string.punctuation)
            #self.cleaned_text = self.text.translate(self.translator).lower()
            #self.text_blob = TextBlob(self.text)
            #self.polarity_score = self.text_blob.sentiment.polarity
            #self.subjectivity_score = self.text_blob.sentiment.subjectivity
            print(self.title)
            #print("Subjectivity: {0}".format(self.subjectivity_score))
            #print("Polarity: {0}".format(self.polarity_score))
            self.results_dict = {
                "title": self.title,
                "link": link,
                "author": self.authors,
                "publish_date": self.publish_date,
                "text": self.text
            }
        except newspaper.ArticleException as e:
            print("Error: Could not download {0} due to {1}".format(link, str(e)))


class ArticleGrabber:
    """
    The ArticleGrabber class takes a list of Google News articles and grabs them
    and parses them into newspaper3k Article objects.

    Attributes:
        links (list): list of hyperlinks to articles
        parsed_articles (list): list of parsed articles


    """
    def __init__(self, links=google_news.GoogleNewsSearch().articles):
        """The __init__ method

        Takes a list of links and grabs them from Google News

        Arguments:
            links (list): List of hyperlinks to news articles
        """
        self.links = links
        self.parsed_articles = []
        self.parse_articles(self.links)
    
    def parse_articles(self, links):
        """The parse articles functions downloads articles
        from a list of hyperlinks and parses them into
        newspaper3k Article objects

        Arguments:
            links (list): List of hyperlinks to news articles

        Notes:
            A warning will be printed if the article was unable
            to be downloaded
        """
        for link in links:
            article = Article(link)
            self.parsed_articles.append(article)

    def to_pandas(self):
        """Create a pandas dataframe from the parsed articles"""
        all_results = []
        for article in self.parsed_articles:
            all_results.append(article.results_dict)
        return DataFrame(all_results)
          

def save_in_spark(hc, df):
    """Saves pandas dataframes into a spark table"""
    df['write_timestamp'] = datetime.datetime.today()

    spark_df = hc.createDataFrame(df)
    spark_df.write.saveAsTable("articles", mode='append', format="parquet")
    return True

def read_from_spark(hc, hive_table="articles"):
    """Reads from Spark"""
    spark_df = hc.sql("select * from articles")
    spark_df.show()
    return True
    
def main():
    parser = argparse.ArgumentParser(description='Search Google News and return Sentiment Analysis')
    parser.add_argument('--search', dest='search', default="", help="The search term to use")
    parser.add_argument('--out', dest='out', default="./google_news_searches",
                        help='The output directory')
    args = parser.parse_args()
    spark_conf = pyspark.SparkConf()
    spark_conf.set('spark.driver.memory', '512m')
    sc = pyspark.SparkContext('local', appName="Article Grabber", conf=spark_conf)
    hc = pyspark.HiveContext(sc)
    df = ArticleGrabber(google_news.GoogleNewsSearch(str(args.search)).articles).to_pandas()
    ts = datetime.datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
    save_in_spark(hc, df)
    read_from_spark(hc)
    sc.stop()

if __name__=="__main__":
    main()
