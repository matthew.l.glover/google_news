import os
from urllib.parse import urlencode, urlparse
from bs4 import BeautifulSoup
from selenium import webdriver

ROOT = os.path.abspath(os.path.dirname(__file__ ))
print(ROOT)


def is_article(link):
    """
    returns True if the link is an article (contains ./articles in link)
    """
    href = link.get("href")
    if href == None:
        returnval = False
    elif "./articles" in href:
        returnval = True
    else:
        returnval = False
    return returnval


class ArticleFinder:
    """
    Finds articles in an html string
    """
    def __init__(self, html):
        self.soup = None
        self.make_soup(html=html)
        self.articles = []
        self.find_articles()
        print("self.articles length is:")
        print(len(self.articles))

    def make_soup(self, html):
        """
        parses html string using BeautifulSoup
        """
        self.soup = BeautifulSoup(html, "lxml")


    def find_articles(self):
        """
        Finds articles in the HTML
        """
        self.articles = []
        for link in self.soup.find_all('a'):
            if is_article(link):
                self.articles.append(link.get('href'))



class GoogleNewsSearch(ArticleFinder):
    """
    This class takes a search term and fetches results from Google News
    """
    base_url = "https://news.google.com/search"

    def __init__(self, search_term="Data Science"):
        self.search_term = search_term
        self.params = {
            'q': self.search_term
        }
        self.url = self.base_url + "?" + str(urlencode(self.params))
        self.driver = webdriver.PhantomJS(
            os.path.join(ROOT, "phantomjs-2.1.1-linux-x86_64/bin/phantomjs"))
        self.driver.get(self.url)
        super(GoogleNewsSearch, self).__init__(self.driver.page_source)
        self.link_fixer(self.articles)

    def link_fixer(self, links):
        parsed_base_url = urlparse(self.base_url)
        new_articles = []
        for link in links:
            new_articles.append(link.replace("./", "https://{0}/".format(parsed_base_url.netloc)))
        new_articles = list(set(new_articles))
        self.articles = new_articles

        

if __name__ == "__main__":
    penguins = GoogleNewsSearch(search_term="Pittsburgh Penguins")
    print(penguins.articles)
